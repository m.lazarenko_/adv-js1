// Theory

// 1. Процес, який дозволяє одному об'єкту базуватися на іншому.

// 2. super() викликається для визова функцій та властивостей батьківського класу/об'єкта.


class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this._salary = salary;
    }

    set name(value){
        this._name = value;
    }

    get name() {
        return this._name;
    }

    set age(value){
        this._age = value;
    }

    get age(){
        return this._age;
    }

    set salary(value){
        this._salary = value;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary(){
        return this._salary * 3;
    }
}



let a = new Programmer("John", 12, 500, ["Ukrainian, English"])
console.log(a)

let b = new Programmer("Sam", 20, 860, ["Ukrainian, GErman, Chinese"])
console.log(b)

let c = new Programmer("Dean", 33, 2000, ["Ukrainian, English, German"])
console.log(c)

let d = new Programmer("Max", 19, 500, ["Ukrainian, English"])
console.log(d)